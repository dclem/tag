import firebase from 'firebase/app'
import 'firebase/database';
import 'firebase/auth';
import 'firebase/firestore';

var config = {
    apiKey: "AIzaSyArP7ycCo1rsPf-W0v9mD0C5VHXRYmfA8s",
    authDomain: "targetproject-c5c3b.firebaseapp.com",
    databaseURL: "https://targetproject-c5c3b.firebaseio.com",
    projectId: "targetproject-c5c3b",
    storageBucket: "targetproject-c5c3b.appspot.com",
    messagingSenderId: "23669021292"
  };
  
  firebase.initializeApp(config);

  export default firebase;