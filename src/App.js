import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import CreateGame from "./components/game/Create";
import GameDetails from "./components/game/Details";
import Dashboard from "./components/dashboard/Dashboard";
import GameList from "./components/game/List";
import withSplashScreen from "./components/helpers/withSplashScreen";

import SignIn from "./components/auth/SignIn";
import SignUp from "./components/auth/SignUp";

import "./App.scss";

class App extends Component {
  state = {
    users: [
      {
        id: 1,
        username: "Damian Clem",
        email: "damian@email.com",
        password: "12345",
        phone: "123-123-1234",
      },
    ],
  };

  installPrompt = null;

  componentDidMount() {
    console.log("Listening for Install prompt");

    window.addEventListener("load", function() {
      setTimeout(function() {
        // This hides the address bar:
        window.scrollTo(0, 1);
      }, 0);
    });

    window.addEventListener("beforeinstallprompt", e => {
      // For older browsers
      e.preventDefault();
      console.log("Install Prompt fired");
      this.installPrompt = e;
      // See if the app is already installed, in that case, do nothing
      if (
        (window.matchMedia &&
          window.matchMedia("(display-mode: standalone)").matches) ||
        window.navigator.standalone === true
      ) {
        return false;
      }
      // Set the state variable to make button visible
      this.setState({
        installButton: true,
      });
    });
  }

  render() {
    return (
      <Router>
        <div className="App">
          <Route exact path="/" render={props => <GameList {...props} />} />
          <Route path="/list" render={props => <GameList {...props} />} />
          <Route path="/list" render={props => <GameList {...props} />} />

          <Route
            path="/creategame"
            render={props => (
              <CreateGame {...props} user={this.state.users[0]} />
            )}
          />
          <Route
            path="/details/:id"
            render={props => <GameDetails {...props} />}
          />
          <Route path="/signin" component={SignIn} />
          <Route path="/signup" component={SignUp} />
        </div>
      </Router>
    );
  }
}

export default withSplashScreen(App);
