import React, { Component } from 'react';
import Navbar from '../layout/Navbar';
import ShowMessageToast from '../helpers/ShowMessageToast'

import firebase from '../../config/fireConfig.js'

class CreateGame extends Component {

  constructor(props) {
    super(props);

    this.state = {
      gameName: '',
      play_to: 0,
      showErrorMessage: false,
      created_on: new Date(),
      owner_id: this.props.user.id
    }

    // this.changeHandler = this.changeHandler.bind(this);
    this.createNewGame = this.createNewGame.bind(this)

  }

  handleValication(gameName) {
    return {
      name: gameName.length === 0
    }
  }

  changeHandler = e => {

    this.setState({
      [e.target.name]: e.target.value
    });

  }

  createNewGame = e => {

    e.preventDefault();
    
    const errors = this.handleValication(this.state.gameName);
    const isDisabled = Object.keys(errors).some(x => errors[x]);

    if (isDisabled) {
      e.preventDefault();
      this.setState({ showErrorMessage: true })

      setTimeout(() => {
        this.setState({ showErrorMessage: false })
        console.log('clear')
      }, 2000)

      return
    }
    
    const db = firebase.firestore();

    db.collection('games').add({
      name: this.state.gameName,
      play_to: 20,
      created_on: new Date(),
      owner_id: this.props.user.id
    }).then(docRef => {
      const page = '/details/' + docRef.id
      this.props.history.push(page)
    });  

  }
1

componentDidMount () {

  }

  componentWillUnmount() {
  }

  render() {

    return (
      <div>
        <Navbar/>
        <div className='page-content header-clear-medium'>

          { this.state.showErrorMessage === true ? <ShowMessageToast message={'Please complete all required fields'}/> : null }

          <div className="content">
            <div className="caption bottom-0">
                <div className="caption-center">

                <form onSubmit={this.createNewGame}>
                    <div className="padding-wrap">
                        <h1 className="center-text uppercase ultrabold fa-2x">CREATE NEW GAME</h1>
                        <p className="center-text font-11 under-heading bottom-30 color-highlight">
                            Create a new game to play
                        </p>
                        <div className="input-style input-style-1 input-required">
                            <span>Game Name</span>
                            <em>(required)</em>
                            <input 
                                type="text" 
                                name="gameName" 
                                placeholder="Game Name" 
                                value={this.state.name}
                                onChange={this.changeHandler}
                            />
                        </div>
                        
                        <div className="clear"></div>
                        <button type="submit"
                            className="button button-full button-m button-round-huge bg-dark-blue bottom-0">PLAY</button>
                    </div>
                    </form>

                    </div>
                </div>
            </div>
        </div>
      </div>
    )

  }

}

export default CreateGame
