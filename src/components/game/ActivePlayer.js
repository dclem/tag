import React, { Component } from 'react'

class ActivePlayer extends Component {
  render () {
    return ( 
        <div>
        <p className='bottom-10 player-score-current'>
          {this.props.score}
          <span className='game-score'> / {this.props.playto}</span>
        </p>
        </div>
    )
  }
}
export default ActivePlayer