import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Navbar from '../layout/Navbar'

import firebase from '../../config/fireConfig.js'

class GameList extends Component {

  constructor(props) {
    super(props);

    let gamesArry = [];
    let i = 0;

    this.state = {
      games: []
    }

    const db = firebase.firestore();
    db.collection("games").where("owner_id", "==", 1)
    .get()
    .then((querySnapshot) => {

      querySnapshot.forEach((doc) => {
        
        gamesArry[i++] = {
          id: doc.id,
          name: doc.data().name,
          date: doc.data().created_on
        };
      });

    }).then(() => {
      this.setState({
        games: gamesArry
      })
    })
    .catch(function(error) {
        console.log("Error getting documents: ", error);
    });

  }

  render () {
    return (
      <div>
        <Navbar />
        <div className='page-content header-clear-medium'>
          <div className='content'>
            <h3 className='bolder top-30 center-text'>Your Game List</h3>
            <p className='center-text'>Here's the list of your games</p>
            <Link
              to='/creategame'
              className='button button-full button-m button-round-huge bg-dark-blue bottom-30'
            >
              New Game
            </Link>
            
              <div className='search-box search-color bg-dark1-dark shadow-tiny round-large bottom-20'>
                <i className='fa fa-search' />
                <input
                  type='text'
                  placeholder='Search Games'
                  data-search=''
                />
              </div>

              <div className='search-results disabled-search-list' />
              <div className='link-list link-list-2 link-list-long-border'>
                {this.state.games.map(game => (
                  <Link to={'/details/'+game.id} params={{ id: game.id }} key={ game.id }>
                    <i className='fa fa-star color-yellow1-dark' />
                    <span>{game.name}</span>
                    <em className='bg-blue2'>{game.progress}</em>
                    <strong>
                      Winner: {game.winner_user_id}
                    </strong>
                    <i className='fa fa-angle-right' />
                  </Link>
                ))}
              </div>
            </div>
          </div>
        </div>
    )
  }
}

export default GameList
