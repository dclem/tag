import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import Navbar from '../../components/layout/Navbar'
import ActivePlayer from '../../components/game/ActivePlayer'
import AddPlayerModal from '../../components/helpers/AddPlayerModal'

import ReactCountdownClock from 'react-countdown-clock'

// import Sound from 'react-sound';
import soundfile from '../../sounds/buzzer.mp3'

import firebase from '../../config/fireConfig.js'

class GameDetails extends Component {
  constructor (props) {
    super(props)

    this.state = {
      gameid: this.props.match.params.id,
      gameName: 'Loading...',
      newGame: false,
      play_to: 5,
      showCounter: false,
      hit: 0,
      hitCount: [],
      shotTimer: 5,
      activePlayer: {
        id: 1,
        name: 'Damian',
        score: 0
      },
      players: []
    }

    this.timer = 0
    this.handleAddUser = this.handleAddUser.bind(this)
  }

  componentDidMount = () => {
    const deviceID = 'e00fce6820d8e5a6735cdede-deviceID'
    const rootRef = firebase
      .database()
      .ref()
      .child('hitcounter')
    const dbRef = rootRef.child(deviceID)

    dbRef.limitToLast(1).on('child_added', snapshot => {
      if (this.state.newGame === true) {
        this.setState({
          hit: 1
        })
        this.updatePlayerScore()
      } else {
        this.setState({ newGame: true })
      }
    })

    this.getGame()
  }

  handleAddUser = (e) => {
    e.preventDefault()
    let activateMenu = document.getElementById('menu-add-player')
    activateMenu.className += ' menu-active'
    
    // console.log('clicked', active);
  }

  handleCloseModal = (e) => {
    e.preventDefault()
    let active = document.getElementById('menu-add-player')
    active.classList.remove('menu-active');
  }

  getGame = () => {
    const { id } = this.props.match.params
    const db = firebase.firestore()
    db.collection('games')
      .doc(id)
      .get()
      .then(querySnapshot => {
        const game = querySnapshot.data()

        this.setState({
          gameName: game.name
        })

        this.getGamePlayers();

      })
      .catch(function (error) {
        console.log('Error getting documents: ', error)
      })
  }

  getGamePlayers = () => {
    
    let gamePlayers = []
    let i = 0;

    const { id } = this.props.match.params

    const db = firebase.firestore()
    db.collection("players").where("game_id", "==", id)
      .get()
      .then((querySnapshot) => {

        querySnapshot.forEach((doc) => {

          gamePlayers[i++] = {
            id: i,
            name: doc.data().name,
            score: 0
          };

          console.log(doc.data());
        
        })
      }).then(() => {
        this.setState({
          players: gamePlayers
        })
      })
      .catch(function (error) {
        console.log('Error getting documents: ', error)
      })
  }


  timerDone = () => {
    this.setState({ showCounter: false })
    let audio = new Audio(soundfile)
    audio.play()
    console.log('timer done')
  }

  setActivePlayer = e => {
    e.preventDefault()

    let playerId = e.currentTarget.dataset.id
    let playerName = e.currentTarget.dataset.player
    const playerScore = e.currentTarget.dataset.score

    this.setState({
      showCounter: true,
      activePlayer: {
        id: playerId,
        name: playerName,
        score: playerScore
      }
    })
  }

  updatePlayerScore = e => {
    let playerId = parseInt(this.state.activePlayer.id) - 1
    const currentActivePlayerScore = this.state.activePlayer.score
    const newScore = +currentActivePlayerScore + 1

    const currentActivePlayState = { ...this.state.activePlayer }

    this.setState({
      activePlayer: {
        ...currentActivePlayState,
        score: newScore
      }
    })

    const players = [...this.state.players]
    const player = { ...players[playerId] }
    player.score = newScore

    players[playerId] = player
    
    this.setState({ players })

    // save score to firebase

    // set state players list
  }

  render () {
    return (
      <div>
        <Navbar />

        <div className='content header-clear-medium'>

          <h2 className='bottom-30 center-text'>{this.state.gameName}</h2>
          <div className='game-details content-box bg-highlight round-medium center-text'>
          
          <h4 className='top-10 player-name'>{this.state.activePlayer.name}</h4>

            {this.state.showCounter === true ? (
          
            <ReactCountdownClock
                seconds={this.state.shotTimer}
                color='#FF00FF'
                weight='4'
                alpha={0.9}
                size={170}
                onComplete={this.timerDone}
              />
            ) : (
              <ActivePlayer
                score={this.state.activePlayer.score}
                player={this.state.activePlayer.name}
                playto={this.state.play_to}
              />
            )}
          </div>

          <h3 className='top-30 center-text'>Players</h3>

          <div className='game-players-list link-list link-list-2 link-list-long-border content'>

          <button onClick={this.handleAddUser} className="button button-full button-m button-round-huge bg-dark-blue top-20">Add Player</button>

            {this.state.players.map(player => (
              <Link
                data-player={player.name}
                data-score={player.score}
                data-id={player.id}
                onClick={this.setActivePlayer}
                to='/'
                key={player.id}
              >
                <i className='fa fa-chess-rook color-light-blue' />
                <span>{player.name}</span>

                <em
                  className={
                    player.active ? 'player-score color-active' : 'player-score'
                  }
                >
                  {player.score} / {this.state.play_to}
                </em>
              </Link>
            ))}

          </div>

        </div>

        <AddPlayerModal players={this.state.players} gameid={ this.state.gameid } history={ this.props.history } />

      </div>
    )
  }
}

export default GameDetails
