import React from 'react'
import {slide as Menu} from 'react-burger-menu'
import { Link } from 'react-router-dom'

class Navbar extends React.Component {
    constructor(props) {
        super(props)
        this.onClick = this
            .onClick
            .bind(this)
    }

    onClick(e) {
        e.preventDefault()
    }

    onChange() {
        // We need this empty handler to suppress React warning: "You provided a
        // `checked` prop to a form field without an `onChange` handler. This will
        // render a read-only field. If the field should be mutable use
        // `defaultChecked`. Otherwise, set either `onChange` or `readOnly`"
    }

    render() {
        return (
            <div>
                <Menu className="nav nav-medium">

                    <div>
                        <img
                            src="https://picsum.photos/200"
                            alt="img"
                            className="preload-image horizontal-center round-image bottom-30 top-30"
                            width="100"/>
                        <h3 className="center-text color-white bolder">Damian Clem</h3>
                        <p className="boxed-text-huge under-heading color-white opacity-90">
                            Some info
                        </p>
                    </div>
                    <div className="divider"></div>

                    <Link id='home' className='page-components' to='/'>
                        <span>Dashboard</span>
                        <i className="fa fa-chevron-right"></i>
                    </Link>
                    <Link id='about' className='page-components' to='/list'>
                        <span>Games</span>
                        <i className="fa fa-chevron-right"></i>
                    </Link>
                    <Link id='contact' className='page-components' to='/signup'>
                        <span>Sign Up</span>
                        <i className="fa fa-chevron-right"></i>
                    </Link>
                    <Link id='contact' className='page-components' to='/signin'>
                        <span>Sign In</span>
                        <i className="fa fa-chevron-right"></i>
                    </Link>

                </Menu>
                <div className='header header-fixed header-logo-app'>
                    <a href="/" className="header-title ultrabold">
                        <span className="color-highlight">TAG</span>
                    </a>
                </div>
            </div>
        )
    }
}

export default Navbar
