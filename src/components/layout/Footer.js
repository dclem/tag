import React, { Component } from 'react'

class Footer extends Component {
  render () {
    return (
        <footer>
          <div className='row'>
            <div className='col-xs-12'>
              <a
                href='/creategame'
                className='btn btn-block btn-lg btn-primary outline'
              >
                New Game
              </a>
            </div>
          </div>
        </footer>
    )
  }
}

export default Footer
