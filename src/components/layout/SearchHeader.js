import React, { Component } from 'react'
import logo from '../../imgs/logo.svg'

class SearchHeader extends Component {
  render () {
    return (
        <header className='header'>
          <img src={logo} className='App-logo' alt='logo' />

          <div className='form-group'>
            <input
              type='text'
              placeholder='Search Games'
              className='form-control'
            />
          </div>


        </header>
    )
  }
}

export default SearchHeader
