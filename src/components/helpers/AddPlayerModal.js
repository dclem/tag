import React, { Component } from 'react'
import ShowMessageToast from '../helpers/ShowMessageToast'
import firebase from '../../config/fireConfig.js'

class AddPlayerModal extends Component {
  constructor (props) {
    super(props)

    console.log('players', this.props.players);

    this.state = {
      gameid: this.props.gameid,
      playerName: '',
      showErrorMessage: false,
      created_on: new Date()
    }
  }

  handleValication (playerName) {
    return {
      name: playerName.length === 0
    }
  }

  changeHandler = e => {
    console.log(e.target.value)
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  handleCloseModal = e => {
    e.preventDefault()
    let activateMenu = document.getElementById('menu-add-player')
    activateMenu.classList.remove('menu-active')
  }

  handleAddPlayer = e => {
    e.preventDefault()

    const errors = this.handleValication(this.state.playerName)
    const isDisabled = Object.keys(errors).some(x => errors[x])

    if (isDisabled) {
      e.preventDefault()

      this.setState({ showErrorMessage: true })

      setTimeout(() => {
        this.setState({ showErrorMessage: false })
      }, 2000)

      return
    }

    const db = firebase.firestore();

    db.collection('players').add({
      name: this.state.playerName,
      game_id: this.state.gameid,
      created_on: new Date(),
    }).then(() => {
      const page = '/details/' + this.state.gameid
      console.log(this.props.history);
      this.props.history.push(page)
    });  

  }

  render () {
    return (
      <div id='menu-add-player' className='menu menu-box-bottom'>
        <form onSubmit={this.handleAddPlayer}>
          <div className='top-30'>
            {this.state.showErrorMessage === true ? (
              <ShowMessageToast
                message={'Please complete all required fields'}
              />
            ) : null}
          </div>

          <div className='close-button' onClick={this.handleCloseModal}>
            <i className='fa fa-times' />
          </div>

          <div className='content caption-center padding-wrap'>
            <h1 className='uppercase ultrabold top-20 center-text'>
              ADD PLAYER
            </h1>
            <p className='font-11 under-heading bottom-20 center-text'>
              Welcome, enter players name
            </p>

            <div className='input-style input-style-1 input-required'>
              <span>Player Name</span>
              <em>(required)</em>
              <input
                type='text'
                name='playerName'
                placeholder='Enter Player Name'
                value={this.state.playerName}
                onChange={this.changeHandler}
              />
            </div>
            <div className='clear' />
            <button
              type='submit'
              className='button button-full button-m button-round-huge bg-dark-blue top-10'
            >
              ADD PLAYER
            </button>
          </div>
        </form>
      </div>
    )
  }
}
export default AddPlayerModal
