import React, { Component } from 'react'

class ShowMessageToast extends Component {

  render () {
    return (
      <div className="content">
      <div className="alert alert-small alert-round-medium bg-red2-dark showMessage">
      <i className="fa fa-times-circle"></i>
      <span>{ this.props.message }</span>
  </div>    
  </div>
    )
  }
}
export default ShowMessageToast
