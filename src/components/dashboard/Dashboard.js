import React, {Component} from 'react'
import Navbar from '../layout/Navbar'
import { Link } from 'react-router-dom'

class Dashboard extends Component {
    render() {
        return (
            <div>
                <Navbar/>

                <div className='page-content header-clear-medium'>
                    <div className='content'>

                        <h3 className='bolder top-30'>Large Link List</h3>
                        <p>
                            These lists allow you to set 2 icons, a title and a small description. Ideal for
                            more text.
                        </p>
                        
                        <div className='link-list link-list-2 link-list-long-border'>
                            <div className='divider bottom-0'/>
                            <Link to='/'>
                                <i className='fa fa-star color-yellow1-dark'/>
                                <span>Ultra Mobile</span>
                                <strong>Flexible, Fast, and Very Powerful</strong>
                                <i className='fa fa-chevron-right'/>
                            </Link>
                            <Link to='/'>
                                <i className='fa fa-heart color-red2-dark'/>
                                <span>Kolor Mobile</span>
                                <em className='bg-blue2-dark'>TRENDING</em>
                                <strong>Colorful, easy to use and beautiful</strong>
                                <i className='fa fa-chevron-right'/>
                            </Link>
                            <Link to='/'>
                                <i className='fa fa-cog color-blue2-dark'/>
                                <span>Apptastic Mobile</span>
                                <strong>Designed to feel iOS like</strong>
                                <i className='fa fa-chevron-right'/>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Dashboard
