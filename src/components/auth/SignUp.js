import React, {Component} from 'react'
import Navbar from '../layout/Navbar'

class SignUp extends Component {
    render() {
        return (
            <div>
                <Navbar/>
                <div className='page-content header-clear-medium'>
                    <div className="content">
                    <div className="caption bottom-0">
                        <div className="caption-center">

                            <div className="padding-wrap">
                                <h1 className="center-text uppercase ultrabold fa-3x">SIGN UP</h1>
                                <p className="center-text font-11 under-heading bottom-30 color-highlight">
                                    Let's get you logged in
                                </p>
                                <div className="input-style has-icon input-style-1 input-required">
                                    <i className="input-icon fa fa-user font-11"></i>
                                    <span>Username</span>
                                    <em>(required)</em>
                                    <input type="name" placeholder="Username"/>
                                </div>
                                <div className="input-style has-icon input-style-1 input-required">
                                    <i className="input-icon fa fa-lock font-11"></i>
                                    <span>Password</span>
                                    <em>(required)</em>
                                    <input type="password" placeholder="Password"/>
                                </div>
                                <div className="clear"></div>
                                <a
                                    href="/"
                                    className="button button-full button-m button-round-huge bg-dark-blue top-30 bottom-0">LOGIN</a>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default SignUp
